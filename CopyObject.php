<?php

  require '../Iteration/iteration.php';

  function printObject($obj) {
    foreach ($obj as $key => $value) {
      echo "Key: " . $key;
      echo PHP_EOL;
      echo "Value: " . $value;
      echo PHP_EOL;
    }
  }
  
  function copyObject($obj, $objClone) {
    foreach ($obj as $key => $value) {
      $objClone->post = $obj->post;
      $objClone->hasPosts = $obj->hasPosts;
    }
  }

  $post1 = new Posts("First post.");
  $post1->hasPosts = false;
  $post2 = new Posts("Second post.");

  copyObject($post1, $post2);
  echo "Begin post2 output.";
  echo PHP_EOL;
  printObject($post2);
  $post1->hasPosts = true;
  copyObject($post1, $post2);
  printObject($post2);

?>
